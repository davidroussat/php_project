<?php
// Initialize global variable
$matches = [];
// Initialize the session
include("head_admin.php");

// Include config file
require_once "connection_database.php";


if(isset($_GET["id"])){
    $sqlQuery = 'SELECT * FROM post WHERE idPost = '.$_GET["id"];
    $myStatement = $mysqlConnection->prepare($sqlQuery);
    $myStatement->execute();
    $allPost = $myStatement->fetchAll(); 
}

// Define variables and initialize with empty values
$title = $datePost = $categoryPost = $descriptionPost = "";
$title_err = $datePost_err = $categoryPost_err = $descriptionPost_err ="";

// Processing form data when form is submitted
if($_SERVER["REQUEST_METHOD"] == "POST"){            
    // Validate title
    if(empty(trim($_POST["titlePost"]))){
        $title_err = "Please enter a title.";
    } elseif(!preg_match('/^[a-zA-Z0-9_]+$/', trim($_POST["titlePost"]))){
        $title_err = "title can only contain letters, numbers, and underscores.";
    } 

    // Validate categoryPost
    if(empty(trim($_POST["categoryPost"]))){
        $categoryPost_err = "Please enter a categoryPost.";
    } elseif(!preg_match('/^[a-zA-Z0-9_]+$/', trim($_POST["categoryPost"]))){
        $categoryPost_err = "categoryPost can only contain letters, numbers, and underscores.";
    }
    
    if(empty(trim($_POST['descriptionPost']))){
        $categoryDescription_err = "this field can't be empty";
    }

    // Check input errors before inserting in database
    if(empty($title_err) && empty($categoryDescription_err) && empty($categoryPost_err)){
        $categoryName = $_POST['categoryPost'];
        $idCategory = mysqli_fetch_row(mysqli_query($link, "SELECT idCategorie FROM categories WHERE categoryName = '$categoryName'"))[0];
        $titlepost = $_POST["titlePost"];
        $date = date("Y-m-d");
        $description = $_POST['descriptionPost'];
        $sql = "UPDATE post SET titlePost = '$titlepost', datePost = '$date', idCategoriePost = $idCategory, descriptionPost = '$description' WHERE idPost = ".$_POST['id'];
        if($link->query($sql) == TRUE){
            header('Location: account.php');  
        } else {
            echo "error 404";
        }

    }

    if(array_key_exists('DELETE_POST', $_POST)) {
        $delete= 'DELETE from post where idPost = '.$_POST['id'];
        $link->query($delete);
        header('Location: home.php');        
        }
  

    // Close connection
    mysqli_close($link);
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Welcome</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <style>
        body{ font: 14px sans-serif; text-align: center; }
    </style>
</head>
<body>
    <div>
    <?php include('header.php') ?>)
    </div>
    <div class="wrapper">
        <div>

        <form action="edit.php" method="GET">
            <div>
                <p class="text"><b>Titre : </b>  
                    <?php 
                if($_SERVER["REQUEST_METHOD"] != "POST"){
                    foreach ($allPost as $post) {
                        ?>
                            <h1> <?php echo $post['idPost']; ?></h1>
                            <h1> <?php echo $post['titlePost']; ?></h1>
                            <h1><?php echo $post['pseudoUser']; ?></h1>
                            <h1><?php echo $post['datePost']; ?></h1>
                            <h1><?php echo $post['descriptionPost']; ?></h1>
                            <h1><?php echo '---------------------------------'; ?></h1>
                        <?php
                    }
                }?>
                </p>
            </div>
        </form> 
        <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
        <input type="submit" name="DELETE_POST" class="btn btn-primary" value="DELETE"> 
                <div class="form-group">
                    <label>Title</label>
                    <input type="text" name="titlePost" class="form-control <?php echo (!empty($title_err)) ? 'is-invalid' : ''; ?>" placeholder="<?php echo isset($post['titlePost']) ? $post['titlePost'] : '' ; ?>" value="<?php echo $title; ?>">
                    <span class="invalid-feedback"><?php echo $title_err; ?></span>
                </div>

                <div class="form-group">
                    <label>Category du post</label>
                    <select type="text" name="categoryPost" class="form-control <?php echo (!empty($categoryPost_err)) ? 'is-invalid' : ''; ?>" placeholder="<?php echo isset($post['categoryName']) ? $post['categoryName'] : '' ; ?>" value="<?php echo $categoryPost; ?>">
                    <?php  
                        $list=mysqli_query($link, "SELECT categoryName FROM categories");  
                        foreach($list as $row)  {
                    ?>
                    <option>  
                            <?php echo $row['categoryName'];?>  
                    </option> 
                    <?php } ?>
                    <span class="invalid-feedback"><?php echo $categoryPost_err; ?></span>
                        </select>
                </div>

                <div class="form-group">
                    <label>DescriptionPost</label>
                    <input type="text" name="descriptionPost" class="form-control" placeholder="<?php echo $post['descriptionPost']; ?>" value="<?php echo $descriptionPost; ?>">
                    <span class="invalid-feedback"><?php echo "$descriptionPost_err"; ?></span>
                </div>
                <input name="id" style="display: none;" value=<?php preg_match('/(?:id=)(\d{1,10})/', $_SERVER['REQUEST_URI'], $matches); echo $matches[1];?>></input>
            <button type="submit">submit</button>
        </form>
    </div>    

</body>
</html>
