<?php
// Initialize the session
include("head_admin.php");

require_once "connection_database.php";

    $sqlQuery = 'SELECT * FROM usertable';

    $postStatement = $mysqlConnection->prepare($sqlQuery);
    $postStatement->execute();
    $posts = $postStatement->fetchAll();
    

    if(array_key_exists('DELETE_POST', $_POST)) {
        $link->query("DELETE FROM post WHERE idUserPost = ".$_POST["POST_ID"]);
        $link->query("DELETE FROM usertable WHERE idUser = ".$_POST["POST_ID"]);
        header('Location: home.php');
    } 

?>


 
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Welcome</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <style>
        body{ font: 14px sans-serif; text-align: center; }
    </style>
</head>
<body>
    <?php include('header.php') ?>

    <div>
            <p class="text"><b>Titre : </b>  
                <?php 
                foreach ($posts as $post) {
                    ?>
                        <div class="card text-white bg-info mb-3" style="width: 18rem;">
                        <div class="card-body">
                        <h1> <?php echo $post['firstname']; ?></h1>
                        <h1><?php echo $post['lastname']; ?></h1>
                        <h1><?php echo $post['pseudo']; ?></h1>
                        <form  method="post">
                        <input type="hidden" name="POST_ID" value="<?php echo $post['idUser']; ?>">
                        <input type="submit" name="DELETE_POST" class="btn btn-primary" value="<?php echo "Delete"; ?>">
                        </form>    

                        </div>
                        </div>
                    <?php
                }?>
            </p>
        </div>

</div>
</body>
</html>