-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le : ven. 10 déc. 2021 à 08:43
-- Version du serveur :  5.7.31
-- Version de PHP : 7.3.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `projetphp`
--

-- --------------------------------------------------------

--
-- Structure de la table `categories`
--

DROP TABLE IF EXISTS `categories`;
CREATE TABLE IF NOT EXISTS `categories` (
  `idCategorie` int(11) NOT NULL AUTO_INCREMENT,
  `categoryName` varchar(50) NOT NULL,
  `categoryDescription` varchar(255) NOT NULL,
  PRIMARY KEY (`idCategorie`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `categories`
--

INSERT INTO `categories` (`idCategorie`, `categoryName`, `categoryDescription`) VALUES
(1, 'Moto', 'Vroum Vroum');

-- --------------------------------------------------------

--
-- Structure de la table `post`
--

DROP TABLE IF EXISTS `post`;
CREATE TABLE IF NOT EXISTS `post` (
  `idPost` int(11) NOT NULL AUTO_INCREMENT,
  `pseudoUser` varchar(20) DEFAULT NULL,
  `titlePost` varchar(20) DEFAULT NULL,
  `datePost` varchar(13) DEFAULT NULL,
  `categoryPost` varchar(20) DEFAULT NULL,
  `descriptionPost` TEXT DEFAULT NULL,
  `idCategoriePost` int(11) DEFAULT NULL,
  `idUserPost` int(11) DEFAULT NULL,
  PRIMARY KEY (`idPost`),
  KEY `idCategoriePost` (`idCategoriePost`),
  KEY `idUserPost` (`idUserPost`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `post`
--

INSERT INTO `post` (`idPost`, `pseudoUser`, `titlePost`, `datePost`, `categoryPost`, `descriptionPost`, `idCategoriePost`, `idUserPost`) VALUES
(1, 'adm', 'Les voyages', '2021-12-04', 'Moto', 'il était un jour', 1, 1),
(2, NULL, 'tadam', '2021-12-06', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `usertable`
--

DROP TABLE IF EXISTS `usertable`;
CREATE TABLE IF NOT EXISTS `usertable` (
  `idUser` int(11) NOT NULL AUTO_INCREMENT,
  `firstname` varchar(20) DEFAULT NULL,
  `lastname` varchar(50) DEFAULT NULL,
  `pseudo` varchar(20) NOT NULL,
  `password` varchar(100) DEFAULT NULL,
  `birthday` datetime DEFAULT NULL,
  `emailAddress` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`idUser`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `usertable`
--

INSERT INTO `usertable` (`idUser`, `firstname`, `lastname`, `pseudo`, `password`, `birthday`, `emailAddress`) VALUES
(1, 'clement', 'augier', 'adm', 'password1234', '2000-05-19 00:00:00', 'clement@mail.com'),
(2, 'prenomdavid', NULL, 'david', '$2y$10$aqbLAp0w8fxcGwpt5NCDaOto1iUdEsrwqSlUkCo7VRE5XsFjpBbM2', NULL, NULL),
(3, 'arnaud', NULL, 'arnaud33', '$2y$10$Uc2f5Sr8zGh8tijgKOppau0exPatq3VWyxGsirnTzLR9hQJFdjmrm', NULL, NULL),
(4, 'jean', NULL, 'jeanmic', '$2y$10$ARcie2jNZJAfzYDzZe53iOhQAXF8or74r1wOyAvaRZJMdgStbAkAu', NULL, NULL),
(5, 'tata', 'titi', 'toto', '$2y$10$ixqt6BV0HcuUvocRscd50OFh.L3BW0U5J6xAXUVqEQ9quBn4c3SMK', NULL, 'titi@toto@gmail'),
(6, 'totp', 'tou', 'ghp', '$2y$10$.cj5vlxbZvOJjeF6aMkjg.TQ92DP26HaRplOuoFMbUn8uewcukVeq', NULL, 'toto@gmail.com'),
(7, 'tescon', 'tescon', 'tescon', '$2y$10$jCA.7epGW3JEAU/uhsOzb.HnZuC1qNmpEfur6uuBszE7ZlvFBJMge', NULL, 'tescon@gmail.c'),
(8, 'testname', 'testslast', 'test', '$2y$10$dpAU33ISZnET6vWNjAo3PeEPqKybOZRJZxyn4RYhVAvndVeW/qS6m', NULL, 'clement_demoussac@hotmail.fr');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
