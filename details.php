<?php
// Initialize the session
include("head_admin.php");

require_once "connection_database.php";


if(isset($_GET['id']))
{
$sqlQuery = 'SELECT * FROM post WHERE idPost = '.$_GET['id'];
} else die ("erreur sur le get");

$myStatement = $mysqlConnection->prepare($sqlQuery);
$myStatement->execute();
$allPost = $myStatement->fetchAll();

$id = $_GET["id"];

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Welcome</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <style>
        body{ font: 14px sans-serif; text-align: center; }
    </style>
</head>
<body>
    <?php include('header.php') ?>

    <form action="details.php" method="GET">
        <div>
            <p class="text">

                <?php 
                foreach ($allPost as $post) {
                    ?>
                        <div class="card text-white bg-info mb-3" style="width: 18rem;">
                        <div class="card-body">
                        <h1> <?php echo $post['titlePost']; ?></h1>
                        <h1><?php echo $post['pseudoUser']; ?></h1>
                        <h1><?php echo $post['datePost']; ?></h1>
                        <h1><?php echo $post['descriptionPost']; ?></h1>
                        <?php if ($_SESSION["id"] == $post["idUserPost"] || $_SESSION["username"] == "admin") {?>
                            <a href="edit.php?id=<?php echo $post['idPost'];?>" class="btn btn-danger ml-3">Update your post</a>
                            <?php }?>
                        </div>
                        </div>
                    <?php
                }?>
            </p>
        </div>
    </form>
</div>
</body>
</html>