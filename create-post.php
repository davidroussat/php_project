<?php
// Initialize the session
include("head_admin.php");

// Include config file
require_once "connection_database.php";

// Define variables and initialize with empty values
$title = $datePost = $categoryPost = $categoryDescription ="";
$title_err = $datePost_err = $categoryPost_err = $categoryDescription_err = "";


// Processing form data when form is submitted
if($_SERVER["REQUEST_METHOD"] == "POST"){            
    // Validate title
    if(empty(trim($_POST["titlePost"]))){
        $title_err = "Please enter a title.";
    } elseif(!preg_match('/^[a-zA-Z0-9_]+$/', trim($_POST["titlePost"]))){
        $title_err = "title can only contain letters, numbers, and underscores.";
    } 
    
    if(empty(trim($_POST['descriptionPost']))){
        $categoryDescription_err = "this field can't be empty";
    }

    // Check input errors before inserting in database
    if(empty($title_err) && empty($categoryDescription_err) && empty($categoryPost_err)){
        $categoryName = $_POST['categoryPost'];
        $idCategory = mysqli_query($link, "SELECT idCategorie FROM categories WHERE categoryName = '$categoryName'");

        $sql = "INSERT INTO post (titlePost, datePost, categoryPost, descriptionPost, idCategoriePost, idUserPost) VALUES (?, ?, ?, ?, ?, ?)";
        
        $stmt = mysqli_prepare($link, $sql);
        if($stmt){
            // Bind variables to the prepared statement as parameters
            mysqli_stmt_bind_param($stmt, "ssssii", $_POST["titlePost"], date("Y-m-d"), $categoryName, $_POST['descriptionPost'], mysqli_fetch_row($idCategory)[0], $_SESSION['id']);

            // Attempt to execute the prepared statement
            if(mysqli_stmt_execute($stmt)){
                // Redirect to login page
                // header("location: create-post.php");

                header('Location: account.php');
            } else{
                echo "Oops! Something went wrong with at this insertion. Please try again later.";
            }
            //Close statement
            mysqli_stmt_close($stmt);
        } 
    }

    // Close connection
    mysqli_close($link);
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Welcome</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <style>
        body{ font: 14px sans-serif; text-align: center; }
    </style>
</head>
<body>
<?php include('header.php') ?>

    <div class="wrapper">
        <div>
        <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
                <div class="form-group">
                    <label>Title</label>
                    <input type="text" name="titlePost" class="form-control <?php echo (!empty($title_err)) ? 'is-invalid' : ''; ?>" value="<?php echo $title; ?>">
                    <span class="invalid-feedback"><?php echo $title_err; ?></span>
                </div>

                <div class="form-group">
                    <label>Category du post</label>
                    <select type="text" name="categoryPost" class="form-control <?php echo (!empty($categoryPost_err)) ? 'is-invalid' : ''; ?>" value="<?php echo $categoryPost; ?>">
                    <?php  
                        $list=mysqli_query($link, "SELECT categoryName FROM categories");  
                        foreach($list as $row)  {
                    ?>
                    <option>  
                            <?php echo $row['categoryName'];?>  
                    </option> 
                    <?php } ?>
                    <span class="invalid-feedback"><?php echo $categoryPost_err; ?></span>
                        </select>
                </div>
                
                <div class="form-group">
                    <label> Contenu du post </label>
                    <textarea type="text" rows="10" name="descriptionPost" class="form-control <?php echo (!empty($categoryDescription_err)) ? 'is-invalid' : ''; ?>" value="<?php echo $categoryDescription; ?>"></textarea>
                </div>
        </div>
        <button type="submit">submit</button>
    </div>    

</body>
</html>
