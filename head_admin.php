<?php
// Initialize the session 
session_start();

if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
    header("location: login.php");
    exit;
} elseif ($_SESSION["username"] == "admin"){
    $edit_post= "Edit post ";
    $edit_user= "Edit user ";
}
?>