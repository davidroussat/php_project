<?php
// Initialize the session
include("head_admin.php");

// Include config file
require_once "connection_database.php";



if(isset($_GET["id"])){
    $sqlQuery = 'SELECT * FROM usertable WHERE idUser = '.$_SESSION["id"];
    $myStatement = $mysqlConnection->prepare($sqlQuery);
    $myStatement->execute();
    $allPost = $myStatement->fetchAll(); 
}

if($_SERVER["REQUEST_METHOD"] == "POST"){

// Processing form data when form is submitted
if(empty(trim($_POST["firstname"]))){
    $firstname_err = "Please enter a firstname.";
} elseif(!preg_match('/^[a-zA-Z]+$/', trim($_POST["firstname"]))){
    $firstname_err = "firstname can only contain letters, numbers, and underscores.";
} 

// Validate lastname
if(empty(trim($_POST["lastname"]))){
    $lastname_err = "Please enter a lastname.";
} elseif(!preg_match('/^[a-zA-Z]+$/', trim($_POST["lastname"]))){
    $lastname_err = "lastname can only contain letters, numbers, and underscores.";
} 




// Validate username
if(empty(trim($_POST["username"]))){
    $username_err = "Please enter a username.";
} elseif(!preg_match('/^[a-zA-Z0-9_]+$/', trim($_POST["username"]))){
    $username_err = "Username can only contain letters, numbers, and underscores.";
} 


// Validate email-adress
if(empty(trim($_POST["emailAddress"]))){
    $emailAddress_err = "Please enter a emailAddress.";
} 

// Validate password
if(empty(trim($_POST["password"]))){
    $password_err = "Please enter a password.";     
} elseif(strlen(trim($_POST["password"])) < 6){
    $password_err = "Password must have atleast 6 characters.";
} else{
    $password = trim($_POST["password"]);
}

// Validate confirm password
if(empty(trim($_POST["confirm_password"]))){
    $confirm_password_err = "Please confirm password.";     
} else{
    $confirm_password = trim($_POST["confirm_password"]);
    if(empty($password_err) && ($password != $confirm_password)){
        $confirm_password_err = "Password did not match.";
    }
}

    // Check input errors before inserting in database
    if(empty($username_err) && empty($firstname_err) && empty($lastname_err) && empty($emailAddress_err) && empty($password_err) && empty($confirm_password_err)){
        $username = $_POST["username"];
        $firstname = $_POST["firstname"];
        $lastname = $_POST["lastname"];
        $email = $_POST["emailAddress"];
        $pass = password_hash($password, PASSWORD_DEFAULT); 

        $sql = "UPDATE usertable SET pseudo = '$username', firstname= '$firstname' , lastname='$lastname', emailAddress='$email', password='$pass'  WHERE idUser = ".$_SESSION['id'];
        if($link->query($sql) == TRUE){
            $_SESSION["username"]  = $username;
            header('Location: home.php');
        } else {
            echo "404";
        }

    }

    // Close connection
    mysqli_close($link);
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <style>
        body{ font: 14px sans-serif; }
        .wrapper{ width: 360px; padding: 20px; }
    </style>
</head>
<body>
    <div class="wrapper">
        <h2>Update your informations account</h2>
        <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
            <div class="form-group">
                <label>Username</label>
                <input type="text" name="username" class="form-control <?php echo (!empty($username_err)) ? 'is-invalid' : ''; ?>" >
                <span class="invalid-feedback"><?php echo $username_err; ?></span>
            </div>    
            <div class="form-group">
                <label>Firstname</label>
                <input type="text" name="firstname" class="form-control <?php echo (!empty($firstname_err)) ? 'is-invalid' : ''; ?>" >
                <span class="invalid-feedback"><?php echo $firstname_err; ?></span>
            </div> 
            <div class="form-group">
                <label>Lastname</label>
                <input type="text" name="lastname" class="form-control <?php echo (!empty($lastname_err)) ? 'is-invalid' : ''; ?>" >
                <span class="invalid-feedback"><?php echo $lastname_err; ?></span>
            </div> 
            <div class="form-group">
                <label>Email Address</label>
                <input type="email" name="emailAddress" aria-describedby="email-help" class="form-control <?php echo (!empty($emailAddress_err)) ? 'is-invalid' : ''; ?>" >
                <span class="invalid-feedback"><?php echo $emailAddress_err; ?></span>
            </div>
            <div class="form-group">
                <label>Password</label>
                <input type="password" name="password" class="form-control <?php echo (!empty($password_err)) ? 'is-invalid' : ''; ?>" >
                <span class="invalid-feedback"><?php echo $password_err; ?></span>
            </div>
            <div class="form-group">
                <label>Confirm Password</label>
                <input type="password" name="confirm_password" class="form-control <?php echo (!empty($confirm_password_err)) ? 'is-invalid' : ''; ?>"  >
                <span class="invalid-feedback"><?php echo $confirm_password_err; ?></span>
            </div>
            <div class="form-group">
                <input type="submit" class="btn btn-primary" value="Submit">
                <input type="reset" class="btn btn-secondary ml-2" value="Reset">
            </div>
            <p>Already have an account? <a href="login.php">Login here</a>.</p>
        </form>
    </div>    
</body>
</html>