<?php
// Initialize the session
include("head_admin.php");

require_once "connection_database.php";

    $sqlQuery = 'SELECT * FROM post';
    $postStatement = $mysqlConnection->prepare($sqlQuery);
    $postStatement->execute();
    $posts = $postStatement->fetchAll();

?>


 
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Welcome</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <style>
        body{ font: 14px sans-serif; text-align: center; }
    </style>
</head>
<body>
    <?php include('header.php') ?>

    <form action="home.php" method="GET">
    <div>
            <p class="text"> 
                <?php 
                foreach ($posts as $post) {
                    $pseudo = $link->query("SELECT pseudo FROM usertable WHERE idUser = ".$post["idUserPost"])->fetch_row()[0];
                    ?>
                        <div class="card text-white bg-info mb-3" style="width: 18rem;">
                        <div class="card-body">
                        <h1> <?php echo $post['titlePost']; ?></h1>
                        <h1><?php echo $pseudo;  ?></h1>
                        <h1><?php echo $post['datePost']; ?></h1>
                        <h1><?php echo $post['descriptionPost']; ?></h1>
      
                        <a href="details.php?id=<?php echo $post['idPost'];?>" class="btn btn-danger ml-3">Go to details</a>

                        <h1><?php if ($_SESSION["username"] == "admin") {?></h1>
                            <p>
                            <a href="edit.php?id=<?php echo $post['idPost'];?>" class="btn btn-danger ml-3"><?php echo $edit_post;?></a>
                            </p>
                            <?php }?>
                        </div>
                        </div>
                    <?php
                }?>
            </p>
        </div>
    </form>
</div>
</body>
</html>